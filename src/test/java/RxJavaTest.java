import cn.gbase.rxdata.TestPojo;
import cn.gbase.rxdata.WordcountConsumer;
import com.github.davidmoten.guavamini.Sets;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import org.davidmoten.rx.jdbc.Database;
import org.davidmoten.rx.jdbc.annotations.Column;
import org.davidmoten.rx.jdbc.annotations.Query;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class RxJavaTest {

    static String driver = "com.mysql.jdbc.Driver";
    static String url = "jdbc:mysql://127.0.0.1:3306/rxdata";
    static String username = "root";
    static String password = "root";
    Database db;

    static String urlfull = url + "?user=" + username + "&password=" + password;

    @Before
    public void init() {
        db = Database.from(urlfull, 20);
    }


    interface TestPojo {
        @Column
        String name();

        @Column
        int id();
    }

    @Test
    public void t00() {
        db.select("select * from t1")
                .autoMap(TestPojo.class)
                .doOnNext(it -> System.out.println("update!!!"))
                .blockingForEach(System.out::println);
    }

    @Test
    public void t01() {
        db.select("select * from t1")
                .autoMap(TestPojo.class)
                .filter(it -> "jonny".equals(it.name()))
                .blockingForEach(System.out::println);
    }

    @Test
    public void t02() {
        db.select("select * from t1")
                .autoMap(TestPojo.class)
                .limit(1)
                .blockingForEach(System.out::println);
    }



    @Test
    public void t03() {
        Map<String, Integer> map = new HashMap<>();
        db.select("select * from t1")
                .autoMap(TestPojo.class)
                .doOnNext(it -> {
                    String word = it.name();
                    if (map.containsKey(word)) {
                        int count = map.get(word) + 1;
                        map.put(word, count);
                    } else {
                        map.put(word, 1);
                    }
                })
                .blockingForEach(System.out::println);
        map.entrySet().forEach(System.out::println);
    }



}
