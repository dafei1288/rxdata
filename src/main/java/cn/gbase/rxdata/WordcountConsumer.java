package cn.gbase.rxdata;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class WordcountConsumer implements Consumer<TestPojo> {
    Map<String, Integer> map = new HashMap<>();
    @Override
    public void accept(TestPojo testPojo) {
        String word = testPojo.getName();
        if (map.containsKey(word)) {
            int count = map.get(word) + 1;
            map.put(word, count);
        } else {
            map.put(word, 1);
        }
    }

    public void trace(){
        map.entrySet().forEach(System.out::println);
    }
}
