package cn.gbase.rxdata;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.function.Consumer;

public class BlankAlterConsumer implements Consumer<TestPojo>,Observerable {
    private List<Observer> lists = Lists.newArrayList();

    private long rowIndex = 0;

    @Override
    public void accept(TestPojo testPojo) {
        ++rowIndex;
        System.out.println(rowIndex +" ...  processing  > "+testPojo.getName()+" ==> "+(testPojo.getName()==null || "null".equals(testPojo.getName())));
        if(testPojo.getName()==null || "null".equals(testPojo.getName())){
            this.change(rowIndex+ " 有空值了 ！！！！");
        }
    }

    @Override
    public void regObserver(Observer observer) {
        lists.add(observer);
    }

    @Override
    public void delObserver(Observer observer){
        lists.remove(observer);
    }

    @Override
    public void change() {
        lists.stream().forEach(it -> it.update());
    }

    @Override
    public void change(String msg) {
        lists.stream().forEach(it -> it.update(msg));
    }
}
