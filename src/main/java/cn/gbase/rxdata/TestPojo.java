package cn.gbase.rxdata;

public class TestPojo {
    private int id;
    private String name;

    @Override
    public String toString() {
        return "TestPojo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TestPojo(){}

    public TestPojo(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
