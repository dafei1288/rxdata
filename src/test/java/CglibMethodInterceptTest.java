import java.lang.reflect.Method;

import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

class Student {

    private String name = "zhangsan";

    public String getStuName() {
        return name;
    }

}

public class CglibMethodInterceptTest {

    public static void main(String[] args) {
        //创建一个Enhancer对象
        Enhancer enchaner = new Enhancer();
        //设置被代理的类
        enchaner.setSuperclass(Student.class);
        //创建一个回调接口
        Callback interceptor = new MethodInterceptor() {

            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy)
                    throws Throwable {
                System.err.println("原方法名是 ： " + method.getName());
                System.err.println("原方法声明的类为 " + method.getDeclaringClass());
                System.err.println("我是 " + (String) proxy.invokeSuper(obj, args));
                System.err.println("我调用结束了");
                return null;
            }
        };
        enchaner.setCallback(interceptor);

        Student student = (Student) enchaner.create();
        student.getStuName();

    }
}
