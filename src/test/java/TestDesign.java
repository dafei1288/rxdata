import cn.gbase.rxdata.*;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestDesign {
    static String driver = "com.mysql.jdbc.Driver";
    static String url = "jdbc:mysql://127.0.0.1:3306/ys";
    static String username = "root";
    static String password = "dafei1288";

    MysqlDataSource ds = new MysqlDataSource();


    @Before
    public void init(){
        ds.setURL(url);
        ds.setUser(username);
        ds.setPassword(password);
    }

    @Test
    public void alterblank(){
        DataStreamTemplate dst = new DataStreamTemplate(ds);
        try (Stream<TestPojo> stm = dst.query("select * from t1", TestPojo.class);
        ){

            //空值处理消费者
            BlankAlterConsumer ba = new BlankAlterConsumer();

            //添加观察者
            Observer ob = new Observer();
            ba.regObserver(ob);

            //处理
            stm.forEach(ba);
            //stm.close();


        }catch(Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void wordcount(){

        DataStreamTemplate dst = new DataStreamTemplate(ds);
        try {
            Stream<TestPojo> stm = dst.query("select * from t1", TestPojo.class);
            WordcountConsumer wc = new WordcountConsumer();
            //word count 注入消费者
            stm.forEach(wc);
            //打印信息
            wc.trace();

        }catch(Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void t3(){
        DataStreamTemplate dst = new DataStreamTemplate(ds);
        try {
            Stream<TestPojo> stm = dst.query("select * from t1", TestPojo.class);
            stm.limit(1).forEach(System.out::println);
        }catch(Exception e){
            e.printStackTrace();
        }
    }



    @Test
    public void t2(){

        DataStreamTemplate dst = new DataStreamTemplate(ds);
        try {
            Stream<Map> stm = dst.query("select * from t1");

//            stm.forEach(System.out::println);

            List<Map> al = stm.filter(it -> {
//                System.out.println(it.get("name"));
                return ("dafei1288").equals(it.get("name"));
            }).collect(Collectors.toList());



            System.out.println("......  filterd ....");
            al.stream().forEach(System.out::println);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Test
    public  void t1() {

        try {
            Class.forName(driver);
            Connection conn = DriverManager.getConnection(url,username,password);

            Statement stat = conn.createStatement();



            ResultSet rs = stat.executeQuery("select * from t1");

            Observer ob = new Observer();
            rs = ResultSetDynamicProxyHandler.newInstanceWithObserver(rs,ob);

            while(rs.next()){

                String  name = rs.getString("name");
                System.out.println(name);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
