package cn.gbase.rxdata;

public interface Observerable {
    public void regObserver(Observer observer);
    public void delObserver(Observer observer);
    public void change();
    public  void change(String msg);

}
